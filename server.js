require('dotenv').config()
const express = require('express')
const app = express()
var port = 5555
const router = require('./routers/index')
const bodyParser = require('body-parser')
const cors = require('cors')

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(express.static( __dirname + "/publics"))
app.use('/', router)

app.listen(process.env.PORT,() => console.log(`server connect port ${process.env.PORT}`))