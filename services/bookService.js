const bookModel = require('../models/bookModel')

function getBookPage() {
    return bookModel.find().countDocuments((err, count) => {
        return count
    })
}
function getBookAll() {
    return bookModel.find().sort({email: 1})
}
function getBookID(id) {
    return bookModel.find({
        _id: id
    })
}
function getEmail(email) {
    return bookModel.find({
        email: email
    })
}
function createBook(obj) {
    return bookModel.create(obj)
}
function updateBook(id, obj) {
    return bookModel.updateOne({
        _id: id
    },obj)
}
function deleteBook(id) {
    return bookModel.deleteOne({
        _id: id
    })
}

module.exports = {
    getBookPage,
    getBookAll,
    getBookID,
    createBook,
    updateBook,
    deleteBook,
    getEmail
}
