import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Cookies from 'js-cookie';

export const PublicRoute = ({ component: Component, ...rest }) => {
  return (
    <Route {...rest} render={props => (
      <Component {...props} />
    )} />
  );
};

export const PrivateRoute = ({ component: Component }) => {
  return (
    <Route
      render={(props) => {
        let componentRender = <Component {...props} />;
        let token = Cookies.get('token')
        // Redirect hàm chuyển trang
        let homeComponent = <Redirect to='/'/>
        //check login
        if(token) {
          return componentRender 
          // Component cần hiển thị 
        }else{
          return homeComponent
        }
      }}
    />
  )
}