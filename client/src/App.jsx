import React from 'react'
import Login from './component/Login'
import Admin from './component/Admin'
import User from './component/User'
import TableUser from './component/TableUser'
import TableBook from './component/TableBook'
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { PrivateRoute, PublicRoute } from './route/index';

function App() {
  return (
    <Router>
      <Switch>
        <PublicRoute exact path="/login" component={Login} />
        <PrivateRoute exact path="/admin" component={Admin} />
        <PrivateRoute exact path="/admin/user/:page/:size" component={TableUser} />
        <PrivateRoute exact path="/admin/book/:page/:size" component={TableBook} />
        <PrivateRoute exact path="/user/:id/:page/:size" component={User} />
      </Switch>
    </Router>
  );
}

export default App;
