import React, { useState } from 'react'
import Navbar from './Navbar'
import DetailUser from './DetailUser'

function User() {
    let [user, setUser] = useState(1)
    let [book, setBook] = useState(0)
    let [email, setEmail] = useState(null)
    return (
        <div className="home-admin">
            <Navbar email={email} user={user} setUser={setUser} book={book} setBook={setBook}/>
            { user ? <DetailUser setEmail={setEmail}/> : null} 
        </div>
    )
}
export default User