import React, { useEffect, useState, useRef } from 'react'
import '../css/Table.css';
import { get, patch, destroy } from '../utils/FetchAPI'
import { useHistory } from 'react-router-dom';
import 'antd/dist/antd.css';
import { Modal } from 'antd';
import '../css/Login.css'

function DetailUser(props) {
    let history = useHistory()
    let [data, setData] = useState([])
    let path = window.location.href.split("/");
    let id = path[4]
    let page = path[5]
    let size = path[6]
    let refEmail = useRef(null)
    let refUsername = useRef(null)
    let refPhone = useRef(null)
    let refPassword = useRef(null)
    let [doneUpdate, setDoneUpdate] = useState(0)
    async function deleteData(id) {
        const remove = await destroy("/author/" + id)
        if(remove.data.value) {
            if(Number(page)) return history.push("/admin/user/page=" + page + "/size=" + size)
            return history.push("/login")
        }
        return alert(remove.data.message)
    }
    useEffect(() => {
        async function fetchData() {
            const res = await get("/author/" + id)
            setData(res.data.value)
            props.setEmail(res.data.value[0].email)
        }
        fetchData()
    }, [doneUpdate])

    const [isModalVisible, setIsModalVisible] = useState(false);
    const showModal = () => {
        setIsModalVisible(true);
    }
    const handleOk = async () => {
        if(refEmail.current.value && refUsername.current.value && refPhone.current.value && refPassword.current.value) {
            if(Number(refPhone.current.value) && Number(refPassword.current.value)) {
                if(refUsername.current.value.trim().length <= 10) {
                    let res = await patch("/author/" + id, {
                        email: refEmail.current.value.trim(),
                        username: refUsername.current.value.trim(),
                        phone: refPhone.current.value.trim(),
                        password: refPassword.current.value.trim()
                    })
                    if(res.status === 200) {
                        if(!res.data.error) {
                            alert(res.data.message)
                            setDoneUpdate(res.data.value.n)
                            refPassword.current.value = ""
                            return setIsModalVisible(false);
                            }
                        return alert(res.data.message)
                    }
                    return alert(res.error)
                }
                return alert("Username chỉ tối đa 10 ký tự")
            }
            return alert("Phone và Password phải là số")
        }
        alert("Không được để trống")
    }
    
    const handleCancel = () => {
        refPassword.current.value = ""
        setIsModalVisible(false);
    }
    let dataUserMax = data && data.length ? data.map((value, index) => {
        return (
            <tbody key={index} className="table-max">
                <tr>
                    <td>{value.email}</td>
                    <td>{value.username}</td>
                    <td>{value.phone}</td>
                    <td>
                        <button className="option-update" onClick={showModal}>
                            Cập nhật
                        </button>
                        <button className="option-delete" onClick={() => {
                            if(window.confirm("Bạn có muốn xóa không ?")) return deleteData(data[0]._id)  
                        }}>Xóa</button>
                    </td>
                </tr>
            </tbody>
        )
    }) : null
    let dataUserMin = data && data.length ? data.map((value, index) => {
        return (
            <tbody key={index} className="table-min">
                <tr>
                    <td>Email</td>
                    <td>{value.email}</td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td>{value.username}</td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>{value.phone}</td>
                </tr>
                <tr>
                    <td>Option</td>
                    <td>
                        <button className="option-update" onClick={showModal}>
                            Cập nhật
                        </button>
                        <button className="option-delete" onClick={() => {
                            if(window.confirm("Bạn có muốn xóa không ?")) return deleteData(data[0]._id)  
                        }}>Xóa</button>
                    </td>
                </tr>
            </tbody>
        )
    }) : null
    return (
        <div className="table">
            <table style={{width: "100%"}}>
                <thead style={{background: "#ced6e0"}}>
                    <tr className="table-max">
                        <th>Email</th>
                        <th>Username</th>
                        <th>Phone</th>
                        <th>Option</th>
                    </tr>
                    <tr className="table-min">
                        <th>Properties</th>
                        <th>Content</th>
                    </tr>
                </thead>
                { dataUserMax }
                { dataUserMin }
            </table>
            { Number(page) ? <button className="option-back" onClick={() => {
                history.push("/admin/user/page=" + page + "/size=" + size)
            }}>Trở lại</button> : null}
            <Modal title="Đăng ký" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
              <div className="signup">
                <div>
                  <p>Email:</p>
                  <input ref={refEmail} type="text" defaultValue={data.length ? data[0].email : null}/>
                </div>
                <div>
                  <p>Username:</p>
                  <input ref={refUsername} type="text" defaultValue={data.length ? data[0].username : null}/>
                </div>
                <div>
                  <p>Phone:</p>
                  <input ref={refPhone} type="text" defaultValue={data.length ? data[0].phone : null}/>
                </div>
                <div>
                  Password:
                  <input ref={refPassword} type="password" placeholder="password"/>
                </div>
              </div>
            </Modal>
        </div>
    )
}

export default DetailUser