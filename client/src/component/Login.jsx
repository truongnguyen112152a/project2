import React, { useState, useRef } from 'react'
import 'antd/dist/antd.css';
import { Form, Input, Button, Modal } from 'antd';
import '../css/Login.css'
import { post } from '../utils/FetchAPI'
import Cookies from 'js-cookie'
import { useHistory } from 'react-router-dom';

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
}
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
}


const Login = () => {
  let history = useHistory()
  let refEmail = useRef(null)
  let refUsername = useRef(null)
  let refPhone = useRef(null)
  let refPassword = useRef(null)
  const onFinish = async (values) => {
    let res = await post("/author/login", {
      email: values.email,
      password: values.password
    })
    if(res.status === 200) {
      if(!res.data.error) {
        alert(res.data.message)
        Cookies.set('token', res.data.token, { expires: 1 });
        if(res.data.value) return history.push("/admin/user/page=1/size=10")
        return history.push("/user/" + res.data.data[0]._id + "/0/size=1")
      }
      return alert(res.data.message)
    } 
  }
  const onFinishFailed = (errorInfo) => {
    alert("Đăng nhập thất bại")
  }
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  }

  const handleOk = async () => {
    if(refEmail.current.value && refUsername.current.value && refPhone.current.value && refPassword.current.value) {
      if(Number(refPhone.current.value) && Number(refPassword.current.value)) {
        if(refUsername.current.value.trim().length <= 10) {
          let res = await post("/author/sign-up", {
            email: refEmail.current.value.trim(),
            username: refUsername.current.value.trim(),
            phone: refPhone.current.value.trim(),
            password: refPassword.current.value.trim()
          })
          if(res.status === 200) {
            if(!res.data.error) {
              alert(res.data.message)
              refEmail.current.value = ""
              refUsername.current.value = ""
              refPhone.current.value = ""
              refPassword.current.value = ""
              return setIsModalVisible(false);
            }
            return alert(res.data.message)
          }
          return alert(res.error)
        }
        return alert("Username chỉ tối đa 10 ký tự")
      }
      return alert("Phone và Password phải là số")
    }
    alert("Không được để trống")
  }

  const handleCancel = () => {
    refEmail.current.value = ""
    refUsername.current.value = ""
    refPhone.current.value = ""
    refPassword.current.value = ""
    setIsModalVisible(false);
  }
  return (
    <div className="login">
      <div className="login-header">
        <h1>The Library</h1>
      </div>
      <div className="login-from">
        <Form
          className="login-form-child"
          {...layout}
          name="basic"
          initialValues={{
            remember: false,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: 'Please input your email!',
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Đăng nhập
            </Button>
            <Button style={{marginLeft: "20px"}} type="primary" onClick={showModal}>
              Đăng ký
            </Button>
            <Modal title="Đăng ký" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
              <div className="signup">
                <div>
                  <p>Email:</p>
                  <input ref={refEmail} type="text" placeholder="email"/>
                </div>
                <div>
                  <p>Username:</p>
                  <input ref={refUsername} type="text" placeholder="username"/>
                </div>
                <div>
                  <p>Phone:</p>
                  <input ref={refPhone} type="text" placeholder="phone"/>
                </div>
                <div>
                  Password:
                  <input ref={refPassword} placeholder="password" type="password"/>
                </div>
              </div>
            </Modal>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

export default Login