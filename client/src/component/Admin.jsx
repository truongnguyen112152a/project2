import React, { useState } from 'react'
import Navbar from './Navbar'
import TableUser from './TableUser'
import TableBook from './TableBook'
import { useHistory } from 'react-router-dom';

function Admin(props) {
    let history = useHistory()
    let [user, setUser] = useState(1)
    let [book, setBook] = useState(0)
    return (
        <div className="home-admin">
            <Navbar user={user} setUser={setUser} book={book} setBook={setBook}/>
            { user ? history.push("/admin/user/page=1/size=10") : null} 
            { book ? <TableBook /> : null}
        </div>
    )
}
export default Admin