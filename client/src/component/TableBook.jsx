import React, { useState, useEffect, useRef } from 'react'
import '../css/Table.css';
import { Pagination, Modal } from 'antd';
import { get, post } from '../utils/FetchAPI'
import { useHistory } from 'react-router-dom';
import Navbar from './Navbar'

function TableBook() {
    let history = useHistory()
    let pageOnURL = window.location.href.split("book")[1].split("/")[1].split("=")[1]
    let sizeOnURL = window.location.href.split("book")[1].split("/")[2].split("=")[1]
    let [totalData, setTotalData] = useState(0)
    let [data, setData] = useState([])
    let [currentPage, setCurrentPage] = useState(pageOnURL)
    let [currentSize, setCurrentSize] = useState(sizeOnURL)
    let [stt, setStt] = useState(((currentPage - 1) * currentSize) + 1)
    let [doneCreate, setDoneCreate] = useState(0)
    useEffect(() => {
        async function fetchData() {
            let res = await get("/book")
            setTotalData(res.data.value)
        }
        fetchData()
    }, [])
    useEffect(() => {
        async function fetchData() {
            let res = await get(`/book/page/${currentPage}/${currentSize}`)
            if (res.status) setData(res.data.value)
        }
        fetchData()
    }, [currentPage, currentSize, doneCreate])
    let dataUser = data && data.length ? data.map((value, index) => {
        return (
            <tr key={index}>
                <td>{stt++}</td>
                <td>{value.email}</td>
                <td>{value.name}</td>
                <td>
                    <button className="option-detail" onClick={() => {
                        history.push("/user/" + value._id + "/" + currentPage + "/" + currentSize)
                    }}>Chi tiết</button>
                </td>
            </tr>
        )
    }) : null
    // function modal
    let date = new Date()
    let refEmail = useRef(null)
    let refName = useRef(null)
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    }
    const handleOk = async () => {
        if (refEmail.current.value && refName.current.value) {
            let res = await post("/book", {
                email: refEmail.current.value.trim(),
                name: refName.current.value.trim(),
                time: date.getTime(),
            })
            if (res.status === 200) {
                if (!res.data.error) {
                    alert(res.data.message)
                    refEmail.current.value = ""
                    refName.current.value = ""
                    setDoneCreate(1)
                    return setIsModalVisible(false);
                }
                return alert(res.data.message)
            }
            return alert(res.error)
        }
        alert("Không được để trống")
    }

    const handleCancel = () => {
        refEmail.current.value = ""
        refName.current.value = ""
        setIsModalVisible(false);
    }
    return (
        <div className="home-admin">
            <Navbar menu={1}/>
            <div className="table">
                <table style={{ width: "100%" }}>
                    <thead style={{ background: "#ced6e0" }}>
                        <tr>
                            <th>STT</th>
                            <th>Email</th>
                            <th>Username</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.length ? dataUser : null}
                    </tbody>
                </table>
                <button className="option-create" onClick={showModal}>Tạo mới</button>
                <div className="pagination">
                    <Pagination
                        defaultCurrent={Number(currentPage)}
                        defaultPageSize={Number(currentSize)}
                        total={totalData}
                        current={Number(currentPage)}
                        responsive={true}
                        showSizeChanger={true}
                        pageSize={Number(currentSize)}
                        onChange={(page, pageSize) => {
                            setCurrentPage(page)
                            setCurrentSize(pageSize)
                            setStt(((page - 1) * pageSize) + 1)
                            history.push("/admin/book/page=" + page + "/size=" + pageSize)
                        }}
                    />
                </div>
                <Modal title="Đăng ký" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <div className="signup">
                        <div>
                            <p>Email:</p>
                            <input ref={refEmail} type="text" placeholder="email" />
                        </div>
                        <div>
                            <p>Name:</p>
                            <input ref={refName} type="text" placeholder="name" />
                        </div>
                    </div>
                </Modal>
            </div>
        </div>
        
    )
}

export default TableBook