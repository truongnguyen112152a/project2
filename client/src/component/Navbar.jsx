import React, { useState } from 'react'
import {UnorderedListOutlined} from '@ant-design/icons'
import '../css/Navbar.css'
import Cookies from 'js-cookie';
import { useHistory } from 'react-router-dom';

function Menu(props) {
    let history = useHistory()
    return (
        <div className="menu">
            <div className={ props.menu ? "menu-user" : "menu-user menu-background"} onClick={() => {
                history.push("/admin/user/page=1/size=10")
            }}>Quản lý người dùng</div>
            <div className={ !props.menu ? "menu-book" : "menu-book menu-background"} onClick={() => {
                history.push("/admin/book/page=1/size=10")
            }}>Quản lý thư viện</div>
        </div>
    )
}

function Navbar(props) {
    let history = useHistory()
    let [showMenu, setShowMenu] = useState(0)
    return (
        <div>
            <div className="navbar">
                <div className="navbar-logo-menu" onClick={() => {
                    if(!showMenu) return setShowMenu(1)
                }}>
                    <UnorderedListOutlined />
                </div>
                <div className="navbar-account" >
                    <div className="navbar-account-username">{props.email ? props.email : "Manage"}</div>
                    <div className="navbar-account-logout" onClick={() => {
                        if(window.confirm("Bạn có muốn đăng xuất không ?")){
                            history.push("/login")
                            Cookies.remove('token');
                        }
                    }}>Đăng xuất</div>
                </div>
                { showMenu ? <Menu menu={props.menu}/> : null }
            </div>
            { showMenu ? <div className="off-background" onClick={() => {
                setShowMenu(0)
            }}></div> : null}
        </div>
        
    )
}
export default Navbar