import React, { useState, useEffect, useRef } from 'react'
import '../css/Table.css';
import { Pagination, Modal } from 'antd';
import { get, post } from '../utils/FetchAPI'
import { useHistory } from 'react-router-dom';
import Navbar from './Navbar'

function TableUser() {
    let history = useHistory()
    let pageOnURL = window.location.href.split("user")[1].split("/")[1].split("=")[1]
    let sizeOnURL = window.location.href.split("user")[1].split("/")[2].split("=")[1]
    let [totalData, setTotalData] = useState(0)
    let [data, setData] = useState([])
    let [currentPage, setCurrentPage] = useState(pageOnURL)
    let [currentSize, setCurrentSize] = useState(sizeOnURL)
    let [stt, setStt] = useState(((currentPage - 1) * currentSize) + 1)
    let [doneCreate, setDoneCreate] = useState(0)
    useEffect(() => {
        async function fetchData() {
            let res = await get("/author/")
            setTotalData(res.data.value)
        }
        fetchData()
    }, [])
    useEffect(() => {
        async function fetchData() {
            let res = await get(`/author/page/${currentPage}/${currentSize}`)
            if (res.status) setData(res.data.value)
        }
        fetchData()
    }, [currentPage, currentSize, doneCreate])
    let dataUser = data && data.length ? data.map((value, index) => {
        return (
            <tr key={stt}>
                <td>{stt++}</td>
                <td>{value.email}</td>
                <td>{value.username}</td>
                <td>
                    <button className="option-detail" onClick={() => {
                        history.push("/user/" + value._id + "/" + currentPage + "/" + currentSize)
                    }}>Chi tiết</button>
                </td>
            </tr>
        )
    }) : null
    // function modal
    let refEmail = useRef(null)
    let refUsername = useRef(null)
    let refPhone = useRef(null)
    let refPassword = useRef(null)
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    }
    const handleOk = async () => {
        if (refEmail.current.value && refUsername.current.value && refPhone.current.value && refPassword.current.value) {
            if (Number(refPhone.current.value) && Number(refPassword.current.value)) {
                if (refUsername.current.value.trim().length <= 10) {
                    let res = await post("/author/sign-up", {
                        email: refEmail.current.value.trim(),
                        username: refUsername.current.value.trim(),
                        phone: refPhone.current.value.trim(),
                        password: refPassword.current.value.trim()
                    })
                    if (res.status === 200) {
                        if (!res.data.error) {
                            alert(res.data.message)
                            refEmail.current.value = ""
                            refUsername.current.value = ""
                            refPhone.current.value = ""
                            refPassword.current.value = ""
                            setDoneCreate(1)
                            return setIsModalVisible(false);
                        }
                        return alert(res.data.message)
                    }
                    return alert(res.error)
                }
                return alert("Username chỉ tối đa 10 ký tự")
            }
            return alert("Phone và Password phải là số")
        }
        alert("Không được để trống")
    }

    const handleCancel = () => {
        refEmail.current.value = ""
        refUsername.current.value = ""
        refPhone.current.value = ""
        refPassword.current.value = ""
        setIsModalVisible(false);
    }
    return (
        <div className="home-admin">
            <Navbar menu={0}/>
            <div className="table">
                <table style={{ width: "100%" }}>
                    <thead style={{ background: "#ced6e0" }}>
                        <tr>
                            <th>STT</th>
                            <th>Email</th>
                            <th>Username</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.length ? dataUser : null}
                    </tbody>
                </table>
                <button className="option-create" onClick={showModal}>Tạo mới</button>
                <div className="pagination">
                    <Pagination
                        defaultCurrent={Number(currentPage)}
                        defaultPageSize={Number(currentSize)}
                        total={totalData}
                        current={Number(currentPage)}
                        responsive={true}
                        showSizeChanger={true}
                        pageSize={Number(currentSize)}
                        onChange={(page, pageSize) => {
                            setCurrentPage(page)
                            setCurrentSize(pageSize)
                            setStt(((page - 1) * pageSize) + 1)
                            history.push("/admin/user/page=" + page + "/size=" + pageSize)
                        }}
                    />
                </div>
                <Modal title="Đăng ký" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <div className="signup">
                        <div>
                            <p>Email:</p>
                            <input ref={refEmail} type="text" placeholder="email" />
                        </div>
                        <div>
                            <p>Username:</p>
                            <input ref={refUsername} type="text" placeholder="username" />
                        </div>
                        <div>
                            <p>Phone:</p>
                            <input ref={refPhone} type="text" placeholder="phone" />
                        </div>
                        <div>
                            Password:
                        <input ref={refPassword} placeholder="password" type="password" />
                        </div>
                    </div>
                </Modal>
            </div>
        </div>
        
    )
}

export default TableUser